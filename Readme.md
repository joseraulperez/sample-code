### Sample code for Interactive Design Foundation

Feature to correct Russian user speech via an _order recognition_ technology.

Files

* Wrapper for the external service `Speech.php` and test `SpeechTest`.

* Api endpoint `SpeechRecognitionController.php` and test `SpeechRecognitionControllerTest.php`.

* Vue Component `ListenAndRepeat.vue` that record the audio and calld the endpoint.