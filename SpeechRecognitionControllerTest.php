<?php

namespace Tests\App\Feature\Api;

use App\Models\Speech;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\UploadedFile;
use Laravel\Passport\Passport;
use Mockery;
use Tests\TestCase;

class SpeechRecognitionControllerTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_calls_recognition_api_with_an_audio_and_return_speech_response()
    {
        Passport::actingAs(factory(User::class)->create());

        // Don't call the paid service to test the api Endpoint.
        $speechMock = Mockery::mock(Speech::class);
        $speechMock
            ->shouldReceive('recognize')
            ->andReturn(['fake-response'])
            ->once();
        $this->app->instance(Speech::class, $speechMock);

        $this->post(route('api.speech_recognition'), [
            'audio' => UploadedFile::fake()->create('fake-file.mp3', 100),
        ])->assertJson(['fake-response']);
    }

    /** @test */
    public function it_calls_recognition_api_without_audio_through_validation_error()
    {
        Passport::actingAs(factory(User::class)->create());

        $this->post(route('api.speech_recognition'), [], [
            'Accept' => 'application/json',
        ])
            ->assertJsonValidationErrors(['audio']);
    }
}
