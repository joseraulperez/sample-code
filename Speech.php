<?php

namespace App\Models;

use Google\Cloud\Speech\SpeechClient;

class Speech {

    /** @var string */
    const PROJECT_ID = 'aprus-1378';

    /** @var string */
    const LANGUAGE_CODE = 'ru-RU';

    /** @var SpeechClient */
    private $client;

    /**
     * Speech constructor.
     *
     * @param SpeechClient $speechClient
     */
    public function __construct(SpeechClient $speechClient)
    {
        $this->setEnvCredentials();

        $this->client = $speechClient;
    }

    /**
     * Try to recognize the sentence.
     * Return an array with the sentence.
     *
     * @param $file
     * @return array
     */
    public function recognize($file)
    {
        $response = $this->client
            ->recognize($file->get(), [
                'projectId' => self::PROJECT_ID,
                'languageCode' => self::LANGUAGE_CODE,
            ]);

        return $this->filterResponse($response);
    }

    private function setEnvCredentials()
    {
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . base_path() . '/aprus-f9e4f029fe19.json');
    }

    private function filterResponse($response)
    {
        if (isset($response[0])) {
            return $response[0]->alternatives()[0]['transcript'];
        }
        return [];
    }
}
