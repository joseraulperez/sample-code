<?php

namespace Tests\App\Unit;

use App\Models\Speech;
use Google\Cloud\Speech\Result;
use Google\Cloud\Speech\SpeechClient;
use Illuminate\Http\UploadedFile;
use Mockery;
use Tests\TestCase;

class SpeechTest extends TestCase
{
    /** @test */
    public function it_recognize_the_audio_and_return_russian_phrase()
    {
        // Don't call the paid service to test the model.
        $result = new Result([
            'alternatives' => [
                [
                    'transcript' => 'Привет',
                    'confidence' => 0.73363745,
                ],
            ],
        ]);

        $speechClientMock = Mockery::mock(SpeechClient::class);
        $speechClientMock
            ->shouldReceive('recognize')
            ->once()
            ->andReturn([$result]);

        $speech = new Speech($speechClientMock);
        $response = $speech->recognize(UploadedFile::fake()->create('fake-file.mp3', 100));

        $this->assertEquals($response, 'Привет');
    }

    /** @test */
    public function it_does_not_recognize_the_audio_and_return_empty_array()
    {
        // Don't call the paid service to test the model.
        $speechClientMock = Mockery::mock(SpeechClient::class);
        $speechClientMock
            ->shouldReceive('recognize')
            ->once()
            ->andReturnNull();

        $speech = new Speech($speechClientMock);
        $response = $speech->recognize(UploadedFile::fake()->create('fake-file.mp3', 100));

        $this->assertEmpty($response);
    }
}
