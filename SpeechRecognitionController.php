<?php

namespace App\Http\Controllers\App\Api;

use App\Http\Controllers\Controller;
use App\Models\Speech;
use Illuminate\Http\Request;

class SpeechRecognitionController extends Controller
{
    /**
     *
     * @param Request $request
     * @param Speech $speech
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, Speech $speech)
    {
        $this->validate($request, [
           'audio' => 'required',
        ]);

        $audioFile = $request->file('audio');

        return response()->json(
            $speech->recognize($audioFile)
        );
    }
}
